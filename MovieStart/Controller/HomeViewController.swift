//
//  HomeViewController.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage

class HomeViewController: UIViewController , UITableViewDelegate, UITableViewDataSource,CustomCellMoview {
    
    private let homeViewmodel = HomeViewModel()
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var tbPopolarMovie: UITableView!
    var listMovie = [ResultPopularModel]()
    let textCellIdentifier = "TableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PresentPopUp(viewcont: self)
        homeViewmodel.getPopular()
        registerTableViewCell()
        tbPopolarMovie.delegate = self
        tbPopolarMovie.dataSource = self
        self.title = "Lo mas popular"
        prepareCallbacks()
        
    }
    private func registerTableViewCell() {
        let cell = UINib(nibName: "TableViewCell", bundle: nil)
        self.tbPopolarMovie.register(cell, forCellReuseIdentifier: "TableViewCell")
    }
    
    private func prepareCallbacks() {
        homeViewmodel.onShowError.subscribe { (Error) in
            PresentMessagePopUp(viewcont: self, mess: Error.element?.status_message ?? "")
            print(Error.element?.status_message ?? "")
        }
        homeViewmodel.onNavigateBack.subscribe(onNext: { (result) in
            DismissPopUp(viewcont: self)
            self.listMovie = result.results!
            self.tbPopolarMovie.reloadData()
        }, onError: { (Error) in
            DismissPopUp(viewcont: self)
            print(Error)
        }, onCompleted: {
            DismissPopUp(viewcont: self)
            print("complete")
        }).disposed(by: self.disposeBag)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! TableViewCell
        cell.ldDescriptionTitle.text = listMovie[indexPath.row].title
        cell.ldDescriptionDate.text = listMovie[indexPath.row].release_date
        cell.tvDescriptionOverview.text = listMovie[indexPath.row].overview
        cell.viewCellListener = self
        cell.MovieInfo =  listMovie[indexPath.row]
        let url = URL(string: "\(const.urlImage)\(listMovie[indexPath.row].poster_path!)")!
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
            size:cell.imgPoster.frame.size,
            radius: 20.0
        )
        (cell.imgPoster).af_setImage(withURL: url, placeholderImage: nil,filter:filter)
        
        
        return cell
    }
    func getSelectedItem(movieinfo: ResultPopularModel) {
        let controller = (self.storyboard?.instantiateViewController(withIdentifier: "DeatilMovie"))! as! DetailViewController
        controller.MovieInfo = movieinfo
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
