//
//  LoginViewController.swift
//  MovieStart
//
//  Created by David Polania on 7/24/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var tfUser: FormTextField!
    @IBOutlet weak var tfPass: FormTextField!
    @IBOutlet weak var btLogin: PrimaryButton!
    
    private let loginViewmodel = LoginViewModel()
    private let disposeBag = DisposeBag()
    
    enum Origin : String{
        case Login  = "Login"
        case Token =  "Token"
    }
    
    var originRequest : Origin = Origin.Login
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfUser.placeholder = "Usuario"
        tfPass.placeholder = "Contraseña"
        tfUser.delegate = self
        tfPass.delegate = self
        btLogin.titleLabel?.text = "Iniciar Sesión"
        loginViewmodel.getToken()
        originRequest = Origin.Login
        prepareCallbacks()
        self.hideKeyboardWhenTappedAround()
    }
    override func viewDidAppear(_ animated: Bool) {
        PresentPopUp(viewcont: self)
    }
    @IBAction func triSendData(_ sender: Any) {
        originRequest = Origin.Token
        let dataLogin =  bodyCreateSessionModel(username: tfUser.text, password: tfPass.text, request_token:  const.apiToken)
        PresentPopUp(viewcont: self)
        loginViewmodel.postSession(obj: dataLogin)
    }
    
    private func prepareCallbacks() {
        loginViewmodel.onShowError.subscribe { (error) in
            DismissPopUp(viewcont: self)
            PresentMessagePopUp(viewcont: self, mess: error.element?.status_message ?? "")
            print(error.element?.status_message! ?? "")
        }
        loginViewmodel.onNavigateBack.subscribe(onNext: { (result) in
            DismissPopUp(viewcont: self)
            switch self.originRequest{
            case Origin.Login:
                    const.apiToken = result.request_token!
                    break
            case Origin.Token:

                let story = UIStoryboard(name: "Home", bundle: nil)
                let controller = story.instantiateViewController(withIdentifier: "NavigationHome")
                self.present(controller, animated: true, completion: nil)
                    break
            }
           
        }, onError: { (Error) in
            DismissPopUp(viewcont: self)
            print(Error)
        }, onCompleted: {
             DismissPopUp(viewcont: self)
            print("complete")
        }).disposed(by: self.disposeBag)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n"{
            dismissKeyboard()
        }
        return true
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
