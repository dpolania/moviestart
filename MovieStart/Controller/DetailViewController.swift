//
//  DetailViewController.swift
//  MovieStart
//
//  Created by David Polania on 7/27/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import AlamofireImage

class DetailViewController: UIViewController {

    @IBOutlet weak var imgBackdrop: UIImageView!
    @IBOutlet weak var tvDetail: UITextView!
    var MovieInfo : ResultPopularModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detalle de la pelicula"
        let url = URL(string: "\(const.urlImage)\((MovieInfo!.backdrop_path ?? MovieInfo!.poster_path ?? "")!)")
        let filter = AspectScaledToFitSizeFilter(
            size:imgBackdrop.frame.size
        )
        (imgBackdrop).af_setImage(withURL: url!, placeholderImage: nil,filter:filter)
        tvDetail.text = MovieInfo?.overview
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
