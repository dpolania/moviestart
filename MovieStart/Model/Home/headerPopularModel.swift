//
//  headerPopularModel.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct headerPopularModel: Codable {
    let page: Int?
    let total_results: Int?
    let total_pages: Int?
    let results: [ResultPopularModel]?
    
    private enum CodingKeys: String, CodingKey {
        case page = "page"
        case total_results = "total_results"
        case total_pages = "total_pages"
        case results = "results"
    }
    
}
