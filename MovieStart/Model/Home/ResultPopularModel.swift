//
//  ResultPopularModel.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct ResultPopularModel: Codable {
    let vote_count: Int?
    let id: Int?
    let video: Bool?
    let vote_average: Double?
    let title: String?
    let popularity: Double?
    let poster_path: String?
    let original_language: String?
    let original_title: String?
    let backdrop_path: String?
    let adult: Bool?
    let overview: String?
    let release_date: String?

    
    private enum CodingKeys: String, CodingKey {
        case vote_count = "vote_count"
        case id = "id"
        case video = "video"
        case vote_average = "vote_average"
        case title = "title"
        case popularity = "popularity"
        case poster_path = "poster_path"
        case original_language = "original_language"
        case original_title = "original_title"
        case backdrop_path = "backdrop_path"
        case adult = "adult"
        case overview = "overview"
        case release_date = "release_date"
    }
    
}
