//
//  TokenModel.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct tokenModelResponse: Codable {
    let success: Bool?
    let expires_at: String?
    let request_token: String?
    
    //Adding custom keys
    private enum CodingKeys: String, CodingKey {
        case success = "success"
        case expires_at = "expires_at"
        case request_token = "request_token"
    }
    
}
