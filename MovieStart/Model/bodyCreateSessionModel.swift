//
//  bodyCreateSessionModel.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import Alamofire

struct bodyCreateSessionModel: Codable {
    let username: String?
    let password: String?
    let request_token: String?
    
    //Adding custom keys
    private enum CodingKeys: String, CodingKey {
        case username = "username"
        case password = "password"
        case request_token = "request_token"
    }
    
    static func parametersObject(data:bodyCreateSessionModel)->Parameters{
        let parameters : Parameters = [
            "username" : data.username!,
            "password":data.password!,
            "request_token":data.request_token!
        ]
        return parameters
    }
    
}
