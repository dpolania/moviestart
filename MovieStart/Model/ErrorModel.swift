//
//  ErrorModel.swift
//  MovieStart
//
//  Created by David Polania on 7/26/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct ErrorModel: Codable {
    let status_code: Int?
    let status_message: String?
    let success: Bool?
    
    //Adding custom keys
    private enum CodingKeys: String, CodingKey {
        case success = "success"
        case status_message = "status_message"
        case status_code = "status_code"
    }
    
}
