//
//  HomeViewModel.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import RxSwift


class HomeViewModel{
    //MARK: - Properties
    private let apiManager: ApiManager
    let disposeBag = DisposeBag()
    
    let onNavigateBack = PublishSubject<headerPopularModel>()
    let onShowError = PublishSubject<ErrorModel>()
    let onShowSuccess = PublishSubject<headerPopularModel>()
    
    //MARK: - Injection ApiManager
    init(apiManager: ApiManager = ApiManager()) {
        self.apiManager = apiManager
    }
    
    func getPopular() {
        self.apiManager.Get(url:ApiServices.popularmovie).subscribe(onNext: { response in
            print(response)
            do { //Converting data to Object
                let responseDecode = try JSONDecoder().decode(headerPopularModel.self, from: response)
                self.onNavigateBack.onNext(responseDecode)
            } catch let error {
                print(error)
                let resultError = ReceiverStatus(response: error)
                self.onShowError.onNext(resultError)
            }
        }, onError: { (error) in
            let resultError = ReceiverStatus(response: error)
            self.onShowError.onNext(resultError)
        }, onCompleted: {
            print("onCompleted")
        }).disposed(by: disposeBag)
    }
}
