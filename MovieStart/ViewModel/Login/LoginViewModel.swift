//
//  LoginViewModel.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import RxSwift

class LoginViewModel {
    
    //MARK: - Properties
    private let apiManager: ApiManager
    let disposeBag = DisposeBag()
    
    let onNavigateBack = PublishSubject<tokenModelResponse>()
    let onShowError = PublishSubject<ErrorModel>()
    let onShowSuccess = PublishSubject<tokenModelResponse>()
    
    //MARK: - Injection ApiManager
    init(apiManager: ApiManager = ApiManager()) {
        self.apiManager = apiManager
    }
    
    //MARK: - Methods
    func getToken() {        
        self.apiManager.Get(url:ApiServices.token).subscribe(onNext: { response in
            print(response)
            do { //Converting data to Object
                let responseDecode = try JSONDecoder().decode(tokenModelResponse.self, from: response)
                self.onNavigateBack.onNext(responseDecode)
            } catch let error {
                print(error)
                self.onShowError.onNext(error as! ErrorModel)
            }
        }, onError: { (error) in
            print(error)
             let resultError = ReceiverStatus(response: error)
            self.onShowError.onNext(resultError)
        }, onCompleted: {
            print("onCompleted")
        }).disposed(by: disposeBag)
    }
    
    func postSession(obj:bodyCreateSessionModel) {
        let params = bodyCreateSessionModel.parametersObject(data: obj)
        self.apiManager.Post(parameters:params , url:ApiServices.login).subscribe(onNext: { response in
            print(response)
            do { //Converting data to Object
                let responseDecode = try JSONDecoder().decode(tokenModelResponse.self, from: response)
                self.onNavigateBack.onNext(responseDecode)
            } catch let error {
                print(error)
                self.onShowError.onNext(error as! ErrorModel)
            }
        }, onError: { (error) in
            let resultError = ReceiverStatus(response: error)
            self.onShowError.onNext(resultError)
        }, onCompleted: {
            print("onCompleted")
        }).disposed(by: disposeBag)
    }
}
