//
//  FormTextField.swift
//  MovieStart
//
//  Created by David Polania on 7/24/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import Material

class FormTextField: TextField {

    override func draw(_ rect: CGRect) {
        self.textColor = MovieStartColor.TextColor
        self.detailColor = MovieStartColor.SelectedColor
        
        self.dividerActiveColor = MovieStartColor.ErrorColor
        self.dividerNormalColor = MovieStartColor.SecondaryColor
        
        self.placeholderActiveColor = MovieStartColor.PrimaryColor
        self.placeholderNormalColor = MovieStartColor.SecondaryColor
        
        self.leftViewActiveColor = MovieStartColor.ErrorColor
        self.leftViewNormalColor = MovieStartColor.ErrorColor
        self.isPlaceholderAnimated = true
    }

}
