//
//  DescriptionTextView.swift
//  MovieStart
//
//  Created by David Polania on 7/26/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit

class DescriptionTextView: UITextView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = UIFont(name:FontUtil.FontLight, size: CGFloat(FontSizeUtil.Description))
        self.textColor = MovieStartColor.TextColor
    }
    
}
