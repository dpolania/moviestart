//
//  PopUpAlertViewController.swift
//  MovieStart
//
//  Created by David Polania on 7/28/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import Lottie

class PopUpAlertViewController: UIViewController {

    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var vwAnimation: UIView!
     let animationView = AnimationView()
    var message = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbMessage.text = message
        
        let animation = Animation.named("1174-warning", subdirectory: "Animations")
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        vwAnimation.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.topAnchor.constraint(equalTo: vwAnimation.topAnchor).isActive = true
        animationView.leadingAnchor.constraint(equalTo: vwAnimation.leadingAnchor).isActive = true
        
        animationView.bottomAnchor.constraint(equalTo: vwAnimation.bottomAnchor, constant: 0).isActive = true
        animationView.trailingAnchor.constraint(equalTo: vwAnimation.trailingAnchor).isActive = true
        animationView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animationView.play(fromProgress: 0,
                           toProgress: 1,
                           loopMode: LottieLoopMode.loop,
                           completion: { (finished) in
                            if finished {
                                print("Animation Complete")
                            } else {
                                print("Animation cancelled")
                            }
        })
        
    }

    @IBAction func triAccept(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
