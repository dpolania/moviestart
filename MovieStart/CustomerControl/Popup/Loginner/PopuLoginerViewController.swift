//
//  PopuLoginerViewController.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import Lottie

class PopuLoginerViewController: UIViewController {
    let animationView = AnimationView()
    static var onDissmis:((_ Result:String)->())?
    
    @IBOutlet weak var vContentAnimation: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let animation = Animation.named("6064-earn-tickets", subdirectory: "Animations")
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        vContentAnimation.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.topAnchor.constraint(equalTo: vContentAnimation.topAnchor, constant: 0).isActive = true
        animationView.leadingAnchor.constraint(equalTo: vContentAnimation.leadingAnchor, constant: 0).isActive = true
        
        animationView.bottomAnchor.constraint(equalTo: vContentAnimation.bottomAnchor, constant: 0).isActive = true
        animationView.trailingAnchor.constraint(equalTo: vContentAnimation.trailingAnchor, constant: 0).isActive = true
        animationView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        PopuLoginerViewController.onDissmis = { Result in
            if Result == "ok"{
                self.dismiss(animated: true, completion: nil)
            }
        }
        //ltvAnimationView.play()
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animationView.play(fromProgress: 0,
                           toProgress: 1,
                           loopMode: LottieLoopMode.loop,
                           completion: { (finished) in
                            if finished {
                                print("Animation Complete")
                            } else {
                                print("Animation cancelled")
                            }
        })
        
    }
}
