//
//  PrimaryButton.swift
//  MovieStart
//
//  Created by David Polania on 7/24/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit

class PrimaryButton: UIButton {


    override func draw(_ rect: CGRect) {
        self.setTitleColor(UIColor.white, for: .normal)
        self.layer.backgroundColor = MovieStartColor.PrimaryColor.cgColor
        //Radius
        self.layer.cornerRadius = self.layer.height / 2
        //Padding
        self.contentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    }
    

}
