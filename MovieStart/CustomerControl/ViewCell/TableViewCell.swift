//
//  TableViewCell.swift
//  MovieStart
//
//  Created by David Polania on 7/26/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit

protocol CustomCellMoview {
    func getSelectedItem(movieinfo: ResultPopularModel)
}

class TableViewCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var ldDescriptionTitle: UILabel!
    @IBOutlet weak var lbTitleDate: UILabel!
    @IBOutlet weak var ldDescriptionDate: UILabel!
    @IBOutlet weak var lbTitleOverview: UILabel!
    @IBOutlet weak var tvDescriptionOverview: UITextView!
    @IBOutlet weak var imgPoster: UIImageView!
    public var viewCellListener: CustomCellMoview?
    var MovieInfo : ResultPopularModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func triSelectCell(_ sender: Any) {
        viewCellListener!.getSelectedItem(movieinfo: MovieInfo!)
    }
    
}
