//
//  PresentPopUp.swift
//  MovieStart
//
//  Created by David Polania on 7/25/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import UIKit
var popupLoginer : UIViewController = UIViewController()
func PresentPopUp(viewcont : UIViewController){
    let popupLoginer = PopuLoginerViewController(nibName: "PopuLoginerViewController", bundle: nil)
    popupLoginer.view.tag = 999
    viewcont.view.addSubview(popupLoginer.view)
    
    //popupLoginer.modalPresentationStyle = .overCurrentContext
    //viewcont.present(popupLoginer, animated: true, completion: nil)
}

func DismissPopUp(viewcont : UIViewController){
    let view = viewcont.view.viewWithTag(999)
    view?.removeFromSuperview()
    //PopuLoginerViewController.onDissmis!("ok")
}

func PresentMessagePopUp(viewcont : UIViewController,mess:String){
    let popupAlert = PopUpAlertViewController(nibName: "PopUpAlertViewController", bundle: nil)
    popupAlert.message = mess
    popupAlert.modalPresentationStyle = .overCurrentContext
    viewcont.present(popupAlert, animated: true, completion: nil)
}
