//
//  MovieStartColor.swift
//  MovieStart
//
//  Created by David Polania on 7/24/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import UIKit
struct MovieStartColor {
    static let PrimaryColor = UIColor.create(255, 87, 34)
    static let SecondaryColor  = UIColor.create(255, 255, 255)
    static let SelectedColor  = UIColor.create(230, 74, 25)
    static let ErrorColor  = UIColor.create(175, 42, 0)
    static let TextColor = UIColor.create(255, 255, 255)
}
extension UIColor {
    static func create(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
}
