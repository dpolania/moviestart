//
//  FontUtil.swift
//  MovieStart
//
//  Created by David Polania on 7/26/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct FontUtil {
    static let FontPrimary = "ArialHebrew"
    static let FontBolt = "ArialHebrew-Bold"
    static let FontLight = "ArialHebrew-Light"
}
