//
//  UIFontSizeUtil.swift
//  MovieStart
//
//  Created by David Polania on 7/26/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct FontSizeUtil {
    static let Title = 18.0
    static let Description = 15.0
    static let Paragraph = 16.0
}
