//
//  ApiParams.swift
//  MovieStart
//
//  Created by David Polania on 7/24/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct ApiParams {

    static let baseURL = "https://api.themoviedb.org/3/"
}

struct ApiServices {
    static let token = "\(ApiParams.baseURL)authentication/token/new?api_key=\(const.apiKey)"
    static let login   = "\(ApiParams.baseURL)authentication/token/validate_with_login?api_key=\(const.apiKey)"
    static let popularmovie   = "\(ApiParams.baseURL)movie/popular?api_key=\(const.apiKey)&language=es-ES&page=1"
}

struct ApiQueryParams {}

//Header fields
enum HttpHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}
struct const{
    static let apiKey = "ac7fe369b98c8c82f1b15787fb335ae6"
    static var apiToken = ""
    static var urlImage = "https://image.tmdb.org/t/p/w500"
}
//Content Type (JSON)
enum ContentType: String {
    case json = "application/json"
}

enum AFAApiError2: Error {
    case forbidden(Int,String)   //Status code 403
    case notFound               //Status code 404
    case conflict               //Status code 409
    case internalServerError    //Status code 500
}
