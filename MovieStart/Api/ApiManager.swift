//
//  ApiManager.swift
//  MovieStart
//
//  Created by David Polania on 7/24/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import RxSwift

class ApiManager{
    enum generalFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    
    func Get(url:String) -> Observable<Data> {
        return Observable.create { (observer) -> Disposable in
            print("url\(url)")
            Alamofire.request(url, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        // if no error provided by alamofire return .notFound error instead.
                        // .notFound should never happen here?
                        observer.onError(response.error ?? generalFailureReason.notFound)
                        return
                    }
                    observer.onNext(data)
                case .failure(let error):
                    print("Failure")
                    print(error)
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func Post(parameters:Parameters,url:String) -> Observable<Data> {
        return Observable.create { (observer) -> Disposable in
            print("url: \(url)")
            print("parameters: \(parameters)")
            let headers: HTTPHeaders = [
                "Content-Type": "application/json;charset=utf-8"
            ]
            
            Alamofire.request(url, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        // if no error provided by alamofire return .notFound error instead.
                        // .notFound should never happen here?
                        observer.onError(response.error ?? generalFailureReason.notFound)
                        return
                    }
                    observer.onNext(data)
                case .failure(let error):
                    print("Failure")
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
}
//Receptor de errores en el servicio
func   ReceiverStatus(response:Error)->ErrorModel{
    print(response.localizedDescription)
    switch response.localizedDescription{
    case "Response status code was unacceptable: 401.":
        let error = ErrorModel(status_code: 401, status_message: "Error de autenticación, verifique password y contraseña", success: false)
        return error
    default:
        let error = ErrorModel(status_code: 500, status_message: "Servicios no disponibles", success: false)
        return error
    }
}
