//
//  MovieStartTests.swift
//  MovieStartTests
//
//  Created by David Polania on 7/24/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import XCTest
import RxSwift
@testable import MovieStart

class MovieStartTests: XCTestCase {

    private let loginViewmodel = LoginViewModel()
    private let disposeBag = DisposeBag()
    private var data : Any?
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let dataLogin =  bodyCreateSessionModel(username: "dpolania91", password: "1991_david", request_token:  const.apiToken)
        loginViewmodel.postSession(obj: dataLogin)
        testPrepareCallbacks()
        XCTAssert(false)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    
    private func testPrepareCallbacks() {
        loginViewmodel.onShowError.subscribe { (error) in
            print("\(error)")
            self.data = error
            XCTAssert(self.data == nil)
        }
        loginViewmodel.onNavigateBack.subscribe(onNext: { (result) in
            self.data = result
            XCTAssert(self.data == nil)
        }, onError: { (Error) in
            print(Error)
        }, onCompleted: {
            print("complete")
        }).disposed(by: self.disposeBag)
    }
}
